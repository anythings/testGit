package com.gongying.testgit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initView();
    }

    private void initView() {
        //本地加了一行

        //remote加了一行
        //本地来一个不同行冲突
       //remote来一个不同行冲突
        // 本地来一个不同行冲突....
        // 本地来一个不同行冲突....

// remote来一个不同行冲突..
    }
}
